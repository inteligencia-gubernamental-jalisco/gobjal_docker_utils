# GOB JAL Docker Utils

## [**Lee los wikis**](https://gitlab.com/inteligencia-gubernamental-jalisco/gobjal_docker_utils/wikis/pages)

## Docker CheatSheet

Listar todos los contenedores que están corriendo  
`docker ps`  
Lista todos los contenedores en el sistema  
`docker ps -a`  
Encontrar la IP de un contenedor  
`docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' container_name_or_id`  
Listar todos los volumenes  
`docker volume ls`  
Inspeccionar un volumen asociado a un contenedor  
`docker inspect --format="{{.Mounts}}" container_name_or_id`  
